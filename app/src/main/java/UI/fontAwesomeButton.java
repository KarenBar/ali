package UI;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by karenbarreto on 24-03-15.
 */
public class fontAwesomeButton extends Button {


    public fontAwesomeButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        // TODO Auto-generated constructor stub
        setTypeFace();
    }



    public void setTypeFace(){

        if (!isInEditMode()) {
            Typeface face=Typeface.createFromAsset(getContext().getAssets(), "fontawesome-webfont.ttf");

            setTypeface(face);
        }
    }
}
