package UI;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by karenbarreto on 24-03-15.
 */
public class openSansEditText extends EditText {


    public openSansEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        // TODO Auto-generated constructor stub
        setTypeFace();
    }



    public void setTypeFace(){

        if (!isInEditMode()) {
            Typeface face=Typeface.createFromAsset(getContext().getAssets(), "OpenSans.ttf");

            setTypeface(face);
        }
    }
}
