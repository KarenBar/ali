package com.example.paytech.alicard;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;


public class Sign_up_Activity extends ActionBarActivity implements View.OnClickListener {


    private Button signUpBtn,emailLoginBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_up_fragment);

        init();

    }

    private void init(){

        signUpBtn = (Button) findViewById(R.id.signUpBtn);
        emailLoginBtn = (Button) findViewById(R.id.emailLoginBtn);
        signUpBtn.setOnClickListener(this);
        emailLoginBtn.setOnClickListener(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

    if (v.getId()==R.id.signUpBtn || v.getId()==R.id.emailLoginBtn){
        Intent intent = new Intent(this, Home_Activity.class);
        startActivity(intent);
    }

    }
}
