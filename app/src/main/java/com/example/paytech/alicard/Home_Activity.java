package com.example.paytech.alicard;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import UI.openSansBoldTextView;


public class Home_Activity extends FragmentActivity implements View.OnClickListener {

    //private DrawerLayout mDrawerLayout;
    private openSansBoldTextView tittle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_activity);

        init();

    }

    private void init(){

      //  mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        tittle = (openSansBoldTextView) findViewById(R.id.tittle);
        displayView(new Home_Fragment());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {



    }

    /**
     * Diplaying fragment view for selected nav drawer list item
     * */
    public void displayView(Fragment fragment) {
        // update the main content by replacing fragments

        if (fragment != null) {
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.frame_container, fragment).commit();
        } else {
            // error in creating fragment
            Log.e("Home_Activity", "Error in creating fragment");
        }
    }

    public void setTitle(String title) {
       tittle.setText(title);
    }
}
