package com.example.paytech.alicard;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import UI.openSansButton;


public class Home_Fragment extends Fragment implements View.OnClickListener {


    private openSansButton chooseRestaurantBtn;
    private Home_Activity activity;

    public Home_Fragment(){


    }

    @Override
    public void onClick(View v) {
        if(v.getId()== R.id.chooseRestaurantBtn){
        activity.displayView(new Choose_Restaurant_Fragment());
        activity.setTitle(activity.getString(R.string.choose_location));
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.home_fragment, container, false);
        activity = (Home_Activity) getActivity();
        init(rootView);
        return rootView;
    }

    public void init(View v){
        chooseRestaurantBtn = (openSansButton) v.findViewById(R.id.chooseRestaurantBtn);
        chooseRestaurantBtn.setOnClickListener(this);
    }
}
