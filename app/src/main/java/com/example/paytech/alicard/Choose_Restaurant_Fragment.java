package com.example.paytech.alicard;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;


public class Choose_Restaurant_Fragment extends Fragment implements View.OnClickListener {


    private Spinner spinnerCity,spinnerComuna;
    public Choose_Restaurant_Fragment(){}

    @Override
    public void onClick(View v) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.restaurants_select, container, false);
        spinnerCity = (Spinner) rootView.findViewById(R.id.spinnerCity);
        spinnerComuna = (Spinner) rootView.findViewById(R.id.spinnerComuna);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                R.layout.spinner_row, R.id.spinnerItem, getResources().getStringArray(R.array.Items));

        spinnerCity.setAdapter(adapter);
        spinnerComuna.setAdapter(adapter);

        return rootView;
    }
}
